//
//  UIImage+ImageEffects.h
//  BlurDemo
//
//  Created by amttgroup on 15-5-18.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (ImageEffects)
- (UIImage*) applyLightEffect;
- (UIImage*) applyExtraLightEffect;
- (UIImage*) applyDarkEffect;
- (UIImage*) applyTintEffectWithColor:(UIColor*) tintColor;
- (UIImage*) applyBlurWithRadius:(CGFloat) blurRadius tintColor:(UIColor*) tintColor saturationDeltaFactor:(CGFloat) saturationDeltaFactor maskImage:(UIImage*) maskImage;
@end

//
//  ViewController.m
//  BlurDemo
//
//  Created by amttgroup on 15-5-18.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "ViewController.h"
#import "UIImage+ImageEffects.h"
@interface ViewController ()
@property (nonatomic,strong) CALayer * layer;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}
- (IBAction)onClick:(id)sender {
    self.layer = [CALayer layer];
    self.layer.frame = CGRectMake(80, 100, 160, 160);
    [self.view.layer addSublayer:self.layer];
    float scale = [UIScreen mainScreen].scale;
    UIGraphicsBeginImageContextWithOptions(self.view.frame.size, YES, scale);
    [self.view drawViewHierarchyInRect:self.view.frame afterScreenUpdates:NO];
    __block UIImage * image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    CGImageRef imageRef = CGImageCreateWithImageInRect(image.CGImage, CGRectMake(self.layer.frame.origin.x*scale, self.layer.frame.origin.y*scale, self.layer.frame.size.width*scale, self.layer.frame.size.height*scale));
    NSLog(@"%s %f %@",__func__,scale,self.layer);
    image = [UIImage imageWithCGImage:imageRef];
    switch (self.segmentedControl.selectedSegmentIndex) {
        case 0:
            image = [image applyBlurWithRadius:50.0f tintColor:[UIColor redColor] saturationDeltaFactor:2.0 maskImage:nil];
            break;
        case 1:
            image = [image applyExtraLightEffect];
            break;
        case 2:
            image = [image applyDarkEffect];
            break;
        case 3:
            image = [image applyTintEffectWithColor:[UIColor yellowColor]];
            break;
        case 4:
            image = [image applyDarkEffect];
            break;
        default:
            break;
    }
    
    
    self.layer.contents = (__bridge id)(image.CGImage);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
